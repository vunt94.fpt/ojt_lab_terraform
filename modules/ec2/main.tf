resource "aws_instance" "my_ec2" {
  ami           = var.my_ami
  instance_type = var.instance_type
  subnet_id = var.subnet_id
  security_groups = [var.security_groups]  
  count = length(var.instance_name)
  associate_public_ip_address = count.index == 0 ? true : false
  tags = {
    Name = var.instance_name[count.index]
  }

}

resource "aws_key_pair" "key_london" {
  key_name = var.key_name
  public_key = var.public_key
}

