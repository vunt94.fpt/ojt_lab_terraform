cidr_block = "10.0.0.0/16"
subnet_cidrs_public = ["10.0.1.0/24", "10.0.2.0/24"]
subnet_cidrs_private = ["10.0.3.0/24", "10.0.4.0/24"]
public_az = "eu-west-1a"
private_az = "eu-west-1b"
region = "eu-west-1"