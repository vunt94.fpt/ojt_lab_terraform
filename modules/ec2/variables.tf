variable "my_ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "security_groups" {
  type = list(string)
}

variable "instance_name" {
  type = list(string)
}

variable "key_name" {
  type = string
}

variable "public_key" {
  type = string
}