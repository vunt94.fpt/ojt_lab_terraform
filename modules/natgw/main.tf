resource "aws_nat_gateway" "my_natgw" {
  subnet_id     = var.private_subnet
  tags = {
    Name = var.aws_nat_gateway
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = var.depends_on_igw
}

