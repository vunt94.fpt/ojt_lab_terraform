variable "cidr_block" {
  type = string
}

variable "subnet_cidrs_private" {
  type = list(string)
}

variable "subnet_cidrs_public" {
  type = list(string)
}

variable "public_az" {
  type = string
}

variable "private_az" {
  type = string
}

variable "region" {
  type = string
}