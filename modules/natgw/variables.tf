variable "private_subnet" {
  type = string
}

variable "aws_nat_gateway" {
  type = string
}

variable "depends_on_igw" {
  type = string
}