variable "cidr_block" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "subnet_cidrs_public" {
  type = list(string)
}

variable "subnet_cidrs_private" {
  type = list(string)
}

variable "az" {
  type = string
}