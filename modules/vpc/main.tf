resource "aws_vpc" "myvpc" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"
}

resource "aws_route_table" "public_rtb" {
  vpc_id = aws_vpc.myvpc.id

  tags = {
    Name = "public"
  }
}

resource "aws_route_table" "private_rtb" {
  vpc_id = aws_vpc.myvpc.id

  tags = {
    Name = "private"
  }
}

resource "aws_subnet" "public" {
  count             = length(var.subnet_cidrs_public)
  vpc_id            = aws_vpc.myvpc.id
  cidr_block        = var.subnet_cidrs_public[count.index]
  availability_zone = var.public_az
}

resource "aws_subnet" "private" {
  count             = length(var.subnet_cidrs_private)
  vpc_id            = aws_vpc.myvpc.id
  cidr_block        = var.subnet_cidrs_private[count.index]
  availability_zone = var.private_az
}

resource "aws_route_table_association" "public" {
  count          = length(aws_subnet.public.*.id)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public_rtb.id
}

resource "aws_route_table_association" "private" {
  count          = length(aws_subnet.private.*.id)
  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private_rtb.id
}

